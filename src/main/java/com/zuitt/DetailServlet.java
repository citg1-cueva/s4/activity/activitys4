package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1943764552249535889L;
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" DetailServlet has been initialized. ");
		System.out.println("******************************************");
	}
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		//	PrintWriter out = res.getWriter();
		//	out.println("<h1>Hotel Information</h1>");
		ServletContext srvContext = getServletContext();
		String branding = srvContext.getInitParameter("branding_name");
		HttpSession session = req.getSession();
		
		//first name
		String fname = System.getProperty("fname");
		
		//last name session
		String lname = (String) session.getAttribute("lname");
		
		//email
		String email = (String) srvContext.getAttribute("email");
		
		//contacts
		String contact = req.getParameter("contact");
		
		
		PrintWriter out = res.getWriter();
		out.println("<div style=\"text-align:center\">"+
					"<h1>"+branding+"</h1>"+
					"<p> Firstname: "+fname+"</p>"+
					"<p> Lastname: "+lname+"</p>"+
					"<p> Email: "+email+"</p>"+
					"<p> Contact: "+contact+"</p>"+
					"</div>"
					);
				
				
	    }
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" DetailServlet has been destroy. ");
		System.out.println("******************************************");
	}
}
