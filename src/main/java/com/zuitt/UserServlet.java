package com.zuitt;

import java.io.IOException;


import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" UserServlet has been initialized. ");
		System.out.println("******************************************");
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res)throws IOException, ServletException {	
		HttpSession session = req.getSession();
		
		//Branding (Ex. welcome to phone book) Context parameter
		ServletContext srvContext = getServletContext();
		
		
		//First Name (Ex. Mark) System Properties
		System.getProperties().put("fname", req.getParameter("fname"));
		
		
		//Last Name (Ex. Cueva) HttpSession
		session.setAttribute("lname", req.getParameter("lname"));
	
		
		//Email (Ex. Mark@g.com) Context
		srvContext.setAttribute("email",req.getParameter("email"));

		
		//Contact (Ex. 091821323) Rewriting vai redirect
		System.getProperties().put("contact", req.getParameter("contact"));
		String contact = System.getProperty("contact");
		res.sendRedirect("detail?contact="+contact);
		
		
		
	}
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" UserServlet has been destroy. ");
		System.out.println("******************************************");
	}
}
